# Tensorflow Object Detection using Java

This is a fundamental work for loading a pretrained model to Java using Tensorflow.

The model is based on SSD mobileNet and it's trained on 19555 samples from 11 classes.

Classes are:

- person,
- car,
- bus,
- truck, 
- moto-bike, which includes both bikes and motorcycles
- boat, essential all small boats including jet skis
- weapon, mainly human hand weapons like rifles, shotguns etc
- helicopter-plane, which includes both airplanes and helicopters
- ship, essentially all big ships 
- mil_vehicle, military vehicles including tanks, special army vehicles etc and
- UAV, which includes mainly small drones quadcopters 

## What does does this repository include

The code in this repo is basically a primary demontration of object detection using 
tensorflow and Java code. It takes an image as input (hardocoded image path) and 
returns a new image with the detected bounding boxes (bboxes), corresponding label for each
 bbox and corresponding detection score.
 
 ## mAP for current model
 
For current model (SSD_025) is:

 AP: 0.40070 (UAV)  
 AP: 0.24810 (boat)  
 AP: 0.51596 (bus)  
 AP: 0.52301 (car)  
 AP: 0.36422 (helicopter-plane)  
 AP: 0.38946 (mil_vehicle)  
 AP: 0.39631 (moto-bike)  
 **AP: 0.54977 (person)**  
 AP: 0.85082 (ship)  
 AP: 0.52082 (truck)  
 AP: 0.23993 (weapon)  
 mAP: 0.45446
 
 it does's not have the best mAP but it has the biggest person class AP I got. 