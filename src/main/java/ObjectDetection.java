import org.tensorflow.SavedModelBundle;
import org.tensorflow.Session;
import org.tensorflow.Tensor;
import org.tensorflow.types.UInt8;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;



public class ObjectDetection {

    public enum ObjectClass {
        //*************************//
        // 0 - Psedo class         //
        // 1 - person              //
        // 2 - car                 //
        // 3 - bus                 //
        // 4 - truck               //
        // 5 - moto-bike           //
        // 6 - boat                //
        // 7 - weapon              //
        // 8 - helicopter-plane    //
        // 9 - ship                //
        // 10 - mil_vehicle        //
        // 11 - UAV                //
        //*************************//
        psedo_class, person, car, bus, truck, moto_bike, boat, weapon, helicopter_plane, ship, mil_vehicle, UAV
    }


    private float[][][] detection_boxes;
    private float[][] detection_scores;
    private float[][] detection_classes;

    private Session s;



    public ObjectDetection(String modelPath) {
        // Loads the model from the resources
        ClassLoader classLoader = ObjectDetection.class.getClassLoader();
        String modelFullPath = classLoader.getResource(modelPath).getFile();
        String crossPlatformPath = (new File(modelFullPath)).getAbsoluteFile().toString();
        SavedModelBundle smb = SavedModelBundle.load(crossPlatformPath, "serve");
        s = smb.session();


    }


    public double score(ObjectClass target_class, BufferedImage bi, float scoreThreshold, boolean saveIMGwithBox,
                        String saveImagePath, String newImageName) {




        double minimum_cost = 1.0;


        int w = bi.getWidth();
        int h = bi.getHeight();
        int bufferSize = w * h * 3;

        ByteBuffer byteBuffer = ByteBuffer.allocate(bufferSize);
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                int pixel = bi.getRGB(j, i);

                byteBuffer.put((byte) ((pixel >> 16) & 0xFF));  // red
                byteBuffer.put((byte) ((pixel >> 8) & 0xFF));  // green
                byteBuffer.put((byte) ((pixel) & 0xFF));  // blue
            }
        }
        byteBuffer.rewind();

        // create a Tensor to hold the results
        Tensor inputTensor = Tensor.create(UInt8.class, new long[]{1, bi.getHeight(), bi.getWidth(), 3}, byteBuffer);

        // actual object detection is performed in here
        List<Tensor<?>> result = s.runner()
                .feed("image_tensor", inputTensor)
                .fetch("detection_boxes")
                .fetch("detection_scores")
                .fetch("detection_classes")
                .fetch("num_detections")
                .run();

        // this model specific (don't change it)
        int numMaxClasses = 100;

        // arrays to pass the results for further use
        float[][][] boxes = new float[1][numMaxClasses][4];
        this.detection_boxes = result.get(0).copyTo(boxes);
        float[][] scores = new float[1][numMaxClasses];
        this.detection_scores = result.get(1).copyTo(scores);
        float[][] classes = new float[1][numMaxClasses];
        this.detection_classes = result.get(2).copyTo(classes);
        float[] n = new float[1];
        float[] numDetections = result.get(3).copyTo(n);

        int numDet = Math.round(numDetections[0]);
//            System.out.println("Number of detected: " + numDet);

        Graphics2D g = (Graphics2D) bi.getGraphics();
        if (saveIMGwithBox) {
            g.drawImage(bi, 0, 0, null);
        }


        for (int i = 0; i < numDet; i++) {
            if (detection_scores[0][i] >= scoreThreshold) {

                if (detection_classes[0][i] == target_class.ordinal()) {
                    double currentScore = 1.0 - detection_scores[0][i];
                    if (minimum_cost >  currentScore) {
                        minimum_cost = currentScore;
                    }
                }


                //Create boxes on the new image
                if (saveIMGwithBox) {
                    System.out.println("-----------------------------------");

                    int ymin = Math.round(detection_boxes[0][i][0] * h);
                    int xmin = Math.round(detection_boxes[0][i][1] * w);
                    int ymax = Math.round(detection_boxes[0][i][2] * h);
                    int xmax = Math.round(detection_boxes[0][i][3] * w);

                    System.out.println("X1 " + xmin + " Y1 " + ymin + " X2 " + xmax + " Y2 " + ymax);
                    System.out.println("Score " + detection_scores[0][i]);
                    System.out.println("Predicted " + detection_classes[0][i]);

                    if (detection_classes[0][i] == ObjectClass.person.ordinal()) {
                        g.setColor(Color.cyan);
                    } else if (detection_classes[0][i] == ObjectClass.car.ordinal()) {
                        g.setColor(Color.green);
                    } else if (detection_classes[0][i] == ObjectClass.truck.ordinal() || detection_classes[0][i] == ObjectClass.bus.ordinal()) {
                        g.setColor(Color.yellow);
                    } else if (detection_classes[0][i] == ObjectClass.moto_bike.ordinal()) {
                        g.setColor(Color.magenta);
                    } else if (detection_classes[0][i] == ObjectClass.boat.ordinal()) {
                        g.setColor(Color.blue);
                    } else {
                        g.setColor(Color.pink);
                    }

                    if (detection_classes[0][i] == target_class.ordinal()) {
                        g.setColor(Color.red);
                    }

                    g.drawRect(xmin, ymin, xmax - xmin, ymax - ymin);
                    String outputText = ObjectClass.values()[(int) detection_classes[0][i]].name() + " " + detection_scores[0][i];
                    g.drawString(outputText, xmin, ymin);
                }
            }
        }


        if (saveIMGwithBox){
            try {
                // Write the results to a new image file
                File directory = new File(saveImagePath);
                if (!directory.exists()) {
                    directory.mkdir();
                }

                ImageIO.write(bi, "PNG", new File(saveImagePath+"/"+newImageName+".png"));
            }catch(IOException e){
                System.err.println("ERROR in saving the image file with the bounded boxes" + e.toString());
            }
        }


        return minimum_cost;
    }


}