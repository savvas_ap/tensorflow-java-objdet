import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;

public class Tester {


    private HashMap<String, BufferedImage> map;

    static final float detectionThres = (float) .2;


    public static void main(String[] args) {

        Tester test = new Tester();
        test.loadAllImages();

        long start = System.nanoTime();
        ObjectDetection detect = new ObjectDetection("saved_model");
        System.out.println("Time to load the model: "+ (System.nanoTime() - start)/ 1000000+ "ms");


        start = System.nanoTime();
        double cost = detect.score(ObjectDetection.ObjectClass.person, test.map.get("DJI_0004a.JPG"), detectionThres, true, "../imageLogs/test_tensorFlow_Model/","Bounded_Boxes_DJI_0004a");
        long elapsedTime = System.nanoTime() - start;


        System.out.println("Score: "+cost+", Time needed: " + (elapsedTime / 1000000) + "ms");

    }


    public void loadAllImages() {
        map = new HashMap<String, BufferedImage>();

        // File representing the folder that you select using a FileChooser
        final File dir = new File("src/main/resources/images/");
        // array of supported extensions (use a List if you prefer)
        final String[] EXTENSIONS = new String[]{"gif", "png", "bmp", "JPG"}; // and other formats you need

        // filter to identify images based on their extensions
        final FilenameFilter IMAGE_FILTER = new FilenameFilter() {

            public boolean accept(final File dir, final String name) {
                for (final String ext : EXTENSIONS) {
                    if (name.endsWith("." + ext)) {
                        return (true);
                    }
                }
                return (false);
            }
        };

        if (dir.isDirectory()) { // make sure it's a directory
            for (final File f : dir.listFiles(IMAGE_FILTER)) {
                try {
                    map.put(f.getName(), ImageIO.read(f));

                    System.out.println("image: " + f.getName() + " successfully loaded");
                } catch (final IOException e) {
                    System.err.println("image: " + f.getName() + " cannot be loaded");
                    System.err.println(e.toString());
                }
            }
        }

    }

}